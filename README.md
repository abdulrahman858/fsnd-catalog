# Catalog Project

This code base was meant for the Udacity course FSND .


## Python Libraries
The code in this repository assumes the following python libraries are installed:
* Flask
* SQLAlchemy
* httplib
* requests
* oauth2client
* redis
* passlib
* flask_bootstrap
## Installing flask_bootstrap
Run Command:
```
pip install flask-bootstrap
```

## Inital setup 
Before running project make sure to set up database by going to project folder path and running:
```
python
```

then create it
```
from application import db
db.create_all()
```