import os
from flask import Flask,jsonify
from flask import render_template
from flask import request
from flask import redirect
from flask_sqlalchemy import SQLAlchemy
from flask import session as login_session
import json
from flask import session as login_session
import random
import string
from oauth2client.client import flow_from_clientsecrets
from oauth2client.client import FlowExchangeError
import httplib2
import json
from flask import make_response
import requests
from flask_bootstrap import Bootstrap


CLIENT_ID = json.loads(
    open('client_secrets.json', 'r').read())['web']['client_id']
APPLICATION_NAME = "Catalog Application"
project_dir = os.path.dirname(os.path.abspath(__file__))
database_file = "sqlite:///{}".format(os.path.join(project_dir, "projectDatabase.db"))

app = Flask(__name__)
Bootstrap(app)

app.config["SQLALCHEMY_DATABASE_URI"] = database_file

db = SQLAlchemy(app)


class Category(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), nullable=False)
    @property
    def serialize(self):
       """Return object data in easily serializeable format"""
       return {
       		'id': self.id,
           'name': self.name,
       }


    def __repr__(self):
        return "<Name: {}>".format(self.name)


class Item(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), nullable=False)
    descrption = db.Column(db.String(300), nullable=True)
    category_id = db.Column(db.Integer, db.ForeignKey('category.id'),
    nullable=False)
    @property
    def serialize(self):
       """Return object data in easily serializeable format"""
       return {
       	    'id': self.id,
           'name': self.name,
           'descrption':self.descrption,
           'category_id':self.category_id
       }
    def __repr__(self):
        return "<Name: {}>".format(self.name)
@app.route('/')
@app.route('/hello')
def HelloWorld():
    if 'username' not in login_session :
        login_session['username']='Guest'
    
    categories = Category.query.all()
    items =Item.query.order_by(Item.id.desc()).limit(10).all()
    return render_template('front.html',categories=categories,items=items,USER=login_session['username'])
@app.route('/login')
def login():
    state = ''.join(random.choice(string.ascii_uppercase + string.digits)for x in xrange(32))
    login_session['state'] = state
    return render_template('login.html',STATE=state)
@app.route("/newCategory", methods=["POST"])
def newCategory():
    if 'username' not in login_session :
        login_session['username']='Guest'
    if request.form:
        category = Category(name=request.form.get("name"))
        db.session.add(category)
        db.session.commit()
    return redirect("/")
@app.route('/<int:id>', methods=['GET'])
def get_Category(id):
    if 'username' not in login_session :
        login_session['username']='Guest'
    categories = Category.query.all()
    category = db.session.query(Category).filter_by(id=id).one()
    items = db.session.query(Item).filter_by(category_id=id).all()
    return render_template('category.html',category=category,categories=categories,items=items,USER=login_session['username'])
@app.route('/<int:catId>/<int:itemId>', methods=['GET'])
def get_Item(catId,itemId):
    if 'username' not in login_session :
        login_session['username']='Guest'
    category = db.session.query(Category).filter_by(id=catId).one()
    item = db.session.query(Item).filter_by(id=itemId).one()
    return render_template('item.html',category=category,item=item,USER=login_session['username'])
@app.route('/<int:catId>/<int:itemId>/edit', methods=['GET'])
def edit_ItemFrontEnd(catId,itemId):
    if 'username' not in login_session :
        login_session['username']='Guest'
    category = db.session.query(Category).filter_by(id=catId).one()
    item = db.session.query(Item).filter_by(id=itemId).one()
    return render_template('edititem.html',category=category,item=item,USER=login_session['username'])
@app.route('/<int:catId>/<int:itemId>/edit', methods=['POST'])
def edit_Item(catId,itemId):
    if 'username' not in login_session :
        login_session['username']='Guest'
    if request.form:
        item = Item(id=request.form.get("id") ,name=request.form.get("name"),descrption=request.form.get("descrption"))
        newItem = db.session.query(Item).filter_by(id=item.id).one()
        newItem.name=item.name
        newItem.descrption = item.descrption 
        db.session.commit()
    return redirect("/"+str(catId)+"/"+str(itemId))
@app.route('/<int:catId>/<int:itemId>/delete', methods=['POST'])
def delete_Item(catId,itemId):
    if 'username' not in login_session :
        login_session['username']='Guest'
    if request.form:
        item = Item(id=request.form.get("id") ,name=request.form.get("name"),descrption=request.form.get("descrption"))
        newItem = db.session.query(Item).filter_by(id=item.id).one()
        db.session.delete(newItem)
        db.session.commit()
    return redirect("/"+str(catId))
@app.route('/<int:catId>/<int:itemId>/delete', methods=['GET'])
def delete_ItemFrontEnd(catId,itemId):
    if 'username' not in login_session :
        login_session['username']='Guest'
    category = db.session.query(Category).filter_by(id=catId).one()
    item = db.session.query(Item).filter_by(id=itemId).one()
    return render_template('deleteitem.html',category=category,item=item,USER=login_session['username'])


@app.route('/<int:id>/newItem', methods=['Post'])
def add_Item(id):
    if 'username' not in login_session :
        login_session['username']='Guest'
    item = Item(name=request.form.get("name"),descrption=request.form.get("descrption"),category_id=request.form.get("category_id"))
    db.session.add(item)
    db.session.commit()
    return redirect("/"+str(id))
@app.route('/gconnect', methods=['POST'])
def gconnect():
    # Validate state token
    if request.args.get('state') != login_session['state']:
        response = make_response(json.dumps('Invalid state parameter.'), 401)
        response.headers['Content-Type'] = 'application/json'
        return response
    # Obtain authorization code
    code = request.data

    try:
        # Upgrade the authorization code into a credentials object
        oauth_flow = flow_from_clientsecrets('client_secrets.json', scope='')
        oauth_flow.redirect_uri = 'postmessage'
        credentials = oauth_flow.step2_exchange(code)
    except FlowExchangeError:
        response = make_response(
            json.dumps('Failed to upgrade the authorization code.'), 401)
        response.headers['Content-Type'] = 'application/json'
        return response

    # Check that the access token is valid.
    access_token = credentials.access_token
    url = ('https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=%s'
           % access_token)
    h = httplib2.Http()
    result = json.loads(h.request(url, 'GET')[1])
    # If there was an error in the access token info, abort.
    if result.get('error') is not None:
        response = make_response(json.dumps(result.get('error')), 500)
        response.headers['Content-Type'] = 'application/json'
        return response

    # Verify that the access token is used for the intended user.
    gplus_id = credentials.id_token['sub']
    if result['user_id'] != gplus_id:
        response = make_response(
            json.dumps("Token's user ID doesn't match given user ID."), 401)
        response.headers['Content-Type'] = 'application/json'
        return response

    # Verify that the access token is valid for this app.
    if result['issued_to'] != CLIENT_ID:
        response = make_response(
            json.dumps("Token's client ID does not match app's."), 401)
        print "Token's client ID does not match app's."
        response.headers['Content-Type'] = 'application/json'
        return response

    stored_access_token = login_session.get('access_token')
    stored_gplus_id = login_session.get('gplus_id')
    if stored_access_token is not None and gplus_id == stored_gplus_id:
        response = make_response(json.dumps('Current user is already connected.'),
                                 200)
        response.headers['Content-Type'] = 'application/json'
        return response

    # Store the access token in the session for later use.
    login_session['access_token'] = credentials.access_token
    login_session['gplus_id'] = gplus_id

    # Get user info
    userinfo_url = "https://www.googleapis.com/oauth2/v1/userinfo"
    params = {'access_token': credentials.access_token, 'alt': 'json'}
    answer = requests.get(userinfo_url, params=params)

    data = answer.json()

    login_session['username'] = data['name']
    login_session['picture'] = data['picture']
    login_session['email'] = data['email']
    return redirect('/')

@app.route('/gdisconnect')
def gdisconnect():
        access_token = login_session.get('access_token')
        if access_token is None:
            print 'Access Token is None'
            response = make_response(json.dumps('Current user not connected.'), 401)
            response.headers['Content-Type'] = 'application/json'
            return response
        print 'In gdisconnect access token is %s', access_token
        print 'User name is: '
        print login_session['username']
        url = 'https://accounts.google.com/o/oauth2/revoke?token=%s' % login_session['access_token']
        h = httplib2.Http()
        result = h.request(url, 'GET')[0]
        print 'result is '+str(result) 
        if result['status'] == '200':
            del login_session['access_token']
            del login_session['gplus_id']
            del login_session['username']
            del login_session['email']
            del login_session['picture']
            response = make_response(json.dumps('Successfully disconnected.'), 200)
            response.headers['Content-Type'] = 'application/json'
            return redirect('/')
        else:
            response = make_response(json.dumps('Failed to revoke token for given user.', 400))
            response.headers['Content-Type'] = 'application/json'
            return response

@app.route('/catalog.json')
def api():  
    ca = Category.query.all()
    categories = ca=[i.serialize for i in ca]
    it = Item.query.all()
    items = it=[i.serialize for i in it]
    for cat in categories:
        tempItems=[]
        for item in items:
            if cat['id'] == item['category_id']: 
                tempItems.append(item)
        cat['items']=tempItems


    response = app.response_class(
        response=json.dumps(categories),
        status=200,
        mimetype='application/json'
    )
    return response

if __name__ == '__main__':
    app.secret_key = 'super secret key'
    app.config['SESSION_TYPE'] = 'filesystem'
    app.debug = True
    #app.run(host='0.0.0.0', port=80)